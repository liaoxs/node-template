#node-template
nodeJs项目，JavaScript同构模板项目。

使用express + react + redux + react-router ，实现服务器渲染+动态路由控制。主要目的是实现react的单页面应用，但不局限于单页面。使用webpack打包运行前后端代码，js采用es6语法。
#How to use
cmd `git clone https://github.com/SmallShrimp/node-tpl.git`<br/>
cmd `npm i`<br/>
cmd `npm run dev`<br/>
browser http://localhost:3000
