import mongo from './mongo';

export default class extendHandle {

    constructor(app) {
        this._app = app;
    }

    useExtend() {
        this._app.useMongo = mongo;
    }
}