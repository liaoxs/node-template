import mongoose from 'mongoose';
import logger from '../../logger';

export default function (config) {
    var mongo = config.mongo;
    var uri = mongo.uri;
    var open = config.open;
    var error = config.error;
    var connected = config.connected;
    var disconnected = config.disconnected;

    var db = mongoose.connect(uri, mongo);
    db.connection.on('open', !!open && open instanceof Function ? open : function () {
        logger.info("打开数据库连接:", uri);
    });
    db.connection.on('error', !!error && error instanceof Function ? error : function (error) {
        logger.debug("数据库连接失败：" + error);
    });
    db.connection.on('connected', !!connected && connected instanceof Function ? connected : function () {
        logger.info("数据库连接成功:", uri);
    });
    db.connection.on('disconnected', !!disconnected && disconnected instanceof Function ? disconnected : function () {
        logger.info("取消数据库连接:", uri);
    });
}
